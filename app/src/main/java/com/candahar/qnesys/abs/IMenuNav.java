package qnesys.candahar.com.smarthotel.abs;

import android.app.Activity;
import android.app.Application;
import android.os.Parcelable;

public interface IMenuNav {

    void nav(Activity app);
}
