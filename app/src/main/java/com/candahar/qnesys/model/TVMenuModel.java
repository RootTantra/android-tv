package qnesys.candahar.com.smarthotel.model;

import android.app.Activity;

import qnesys.candahar.com.smarthotel.abs.IMenuNav;

public class TVMenuModel {
    private String title;
    private IMenuNav menuNav;

    public TVMenuModel(String title, IMenuNav menuNav) {
        this.title = title;
        this.menuNav = menuNav;
    }

    public String getTitle() {
        return title;
    }

    public void NavToActivity(Activity activity) {
        menuNav.nav(activity);
    }
}
