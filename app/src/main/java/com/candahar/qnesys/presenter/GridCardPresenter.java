package qnesys.candahar.com.smarthotel.presenter;

import android.app.Activity;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.leanback.widget.Presenter;
import qnesys.candahar.com.smarthotel.R;
import qnesys.candahar.com.smarthotel.model.TVMenuModel;

public class GridCardPresenter extends Presenter {

    private Activity activity ;
    private int cardWidth, cardHeight;

    public GridCardPresenter(Activity activity, int cardWidth, int cardHeight) {
        this.activity = activity;
        this.cardWidth = cardWidth;
        this.cardHeight = cardHeight;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        TextView view = new TextView(parent.getContext());

        Resources res = parent.getResources();

        int width = res.getDimensionPixelSize(R.dimen.grid_item_width);
        int height = res.getDimensionPixelSize(R.dimen.grid_item_height);

        view.setLayoutParams(new ViewGroup.LayoutParams(width,height));
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setBackgroundColor(ContextCompat.getColor(parent.getContext(),
                R.color.tv_menu_default_color));

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        TVMenuModel model = (TVMenuModel) item;
        TextView holder = (TextView)viewHolder.view;

        holder.setText(model.getTitle());

        holder.setOnClickListener(v -> model.NavToActivity(activity));
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
    }
}
