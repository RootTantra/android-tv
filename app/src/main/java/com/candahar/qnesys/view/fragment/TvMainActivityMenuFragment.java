package com.candahar.qnesys.view.fragment;

import android.os.Bundle;
import android.os.Handler;

import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnItemViewClickedListener;
import androidx.leanback.widget.OnItemViewSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.Row;
import androidx.leanback.widget.RowPresenter;
import androidx.leanback.widget.VerticalGridPresenter;
import qnesys.candahar.com.smarthotel.abs.QVerGridFragment;
import qnesys.candahar.com.smarthotel.model.TVMenuModel;
import qnesys.candahar.com.smarthotel.presenter.GridCardPresenter;

public class TvMainActivityMenuFragment extends QVerGridFragment implements OnItemViewSelectedListener, OnItemViewClickedListener {

    private static final int NUM_COLUMNS = 1;
    private ArrayObjectAdapter arrayAdapter;
    private ArrayList<TVMenuModel> menuModels;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            prepareEntranceTransition();
        }

        menuModels = getMenu();
        setupFragment();
    }

    private ArrayList<TVMenuModel> getMenu() {

        ArrayList<TVMenuModel> data = new ArrayList<>();
        data.add(new TVMenuModel("Hotel Information", app -> {

        }));

        data.add(new TVMenuModel("Hotel Facilities", app -> {

        }));

        data.add(new TVMenuModel("Arround Hotel", app -> {

        }));

        data.add(new TVMenuModel("Food & Beverages", app -> {

        }));

        data.add(new TVMenuModel("TV Channel", app -> {

        }));

        data.add(new TVMenuModel("YouTube & Cast", app -> {

        }));

        data.add(new TVMenuModel("Iklan", app -> {

        }));


        return  data;
    }

    private void setupFragment() {
        arrayAdapter = new ArrayObjectAdapter(new GridCardPresenter(getActivity(),100,100));

        VerticalGridPresenter gridPresenter = new VerticalGridPresenter();
        gridPresenter.setNumberOfColumns(NUM_COLUMNS);
        setGridPresenter(gridPresenter);

        arrayAdapter.addAll(0,menuModels);

        new Handler().postDelayed(this::startEntranceTransition, 500);

        setOnItemViewClickedListener(this);
        setOnItemViewSelectedListener(this);
    }

    @Override
    public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {

    }

    @Override
    public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {

    }
}
