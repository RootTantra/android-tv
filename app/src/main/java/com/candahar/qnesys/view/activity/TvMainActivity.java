package com.candahar.qnesys.view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import qnesys.candahar.com.smarthotel.R;
import qnesys.candahar.com.smarthotel.abs.QFragmentActivity;

public class TvMainActivity extends QFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tv_main_activity);
    }
}
